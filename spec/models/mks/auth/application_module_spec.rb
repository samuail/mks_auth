require 'rails_helper'

module Mks
  module Auth
    RSpec.describe ApplicationModule, type: :model do
      it 'has a valid factory' do
        expect(create(:application_module)).to be_valid
      end

      it 'is invalid with no code' do
        expect(build(:application_module, :code => nil)).not_to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:application_module, :code => nil)).not_to be_valid
      end

      it 'is invalid with duplicate code' do
        am = create(:application_module)
        expect(build(:application_module, :code => am.code)).not_to be_valid
      end

      it 'can access users' do
        am = create(:application_module)
        2.times { create(:user, :application_module => am) }
        expect(am.users.count).to eq 2
      end

      it 'can access menus' do
        am = create(:application_module)
        2.times { create(:menu, :application_module => am) }
        expect(am.menus.count).to eq 2
      end
    end
  end
end
