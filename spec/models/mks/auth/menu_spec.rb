require 'rails_helper'

module Mks
  module Auth
    RSpec.describe Menu, type: :model do
      it 'has a valid factory' do
        expect(create(:menu)).to be_valid
      end

      it 'can access user roles' do
        m = create(:user_role, :with_menu).menus.first
        expect(m.roles.count).to eq 1
      end
    end
  end
end
