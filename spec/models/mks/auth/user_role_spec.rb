require 'rails_helper'

module Mks
  module Auth
    RSpec.describe UserRole, type: :model do
      it 'has a valid factory' do
        expect(create(:user_role)).to be_valid
      end

      it 'is invalid with no name' do
        expect(build(:user_role, name: nil)).not_to be_valid
      end

      it 'is invalid with duplicate name' do
        ur = create(:user_role)
        expect(build(:user_role, name: ur.name)).not_to be_valid
      end

      it 'can access users' do
        u = create(:user, :with_role)
        role = u.roles.first
        expect(role.users.count).to eq 1
      end

      it 'can access menus' do
        ur = create(:user_role, :with_menus)
        expect(ur.menus.count).to eq 3
      end
    end
  end
end

