require 'rails_helper'

module Mks
  module Auth
    RSpec.describe User, type: :model do
      it 'has a valid factory' do
        expect(create(:user)).to be_valid
      end

      it 'is invalid with no first name' do
        expect(build(:user, first_name: nil)).not_to be_valid
      end

      it 'is invalid with no last name' do
        expect(build(:user, last_name: nil)).not_to be_valid
      end

      it 'is invalid with no email' do
        expect(build(:user, email: nil)).not_to be_valid
      end

      it 'is invalid with password less than 6 characters' do
        expect(build(:user, password: '1234')).not_to be_valid
      end

      it 'is invalid with duplicate email' do
        u = create(:user)
        expect(build(:user, email: u.email)).not_to be_valid
      end

      it 'is invalid with no active value' do
        expect(build(:user, active: nil)).not_to be_valid
      end

      it 'is invalid with invalid email' do
        expect(build(:user, email: '123456.com')).not_to be_valid
      end

      it 'can access user roles' do
        u = create(:user, :with_roles)
        expect(u.roles.count).to eq 3
      end
    end
  end
end

