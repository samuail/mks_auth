FactoryGirl.define do
  factory :menu, class: 'Mks::Auth::Menu' do
    text { FFaker::Name.name }
    icon_cls { FFaker::Name.name }
    class_name { FFaker::Name.name }
    location { FFaker::Name.name }
    parent nil
    association :application_module
  end
end