FactoryGirl.define do
  factory :user, class: 'Mks::Auth::User' do
    first_name { FFaker::Name.name }
    last_name { FFaker::Name.name }
    email { FFaker::Internet.email }
    active true
    password { FFaker::Internet.password(min_length=6) }
    association :application_module

    trait :with_role do
      after(:create) do |user|
        user.roles << create(:user_role, name: 'role1')
      end
    end

    trait :with_role_and_menus do
      after(:create) do |user|
        user.roles << create(:user_role, :with_menus)
      end
    end

    trait :with_roles do
      after(:create) do |user|
        user.roles = [create(:user_role, name: 'role1'), create(:user_role, name: 'role2'), create(:user_role, name: 'role3')]
      end
    end
  end
end