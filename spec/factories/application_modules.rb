FactoryGirl.define do
  factory :application_module, class: 'Mks::Auth::ApplicationModule' do
    code { FFaker::Name.name }
    name { FFaker::Name.name }
  end
end