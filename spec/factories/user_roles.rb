FactoryGirl.define do
  factory :user_role, class: 'Mks::Auth::UserRole' do
    name { FFaker::Name.name }

    trait :with_menu do
      after(:create) do |role|
        role.menus << create(:menu, text: 'menu1')
      end
    end

    trait :with_menus do
      after(:create) do |role|
        role.menus = [create(:menu, text: 'menu1'), create(:menu, text: 'menu2'), create(:menu, text: 'menu3')]
      end
    end
  end
end
