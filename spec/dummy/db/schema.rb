# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161029074023) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "mks_auth_application_modules", force: :cascade do |t|
    t.string   "code",       null: false
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mks_auth_menus", force: :cascade do |t|
    t.string   "text",                  null: false
    t.string   "icon_cls"
    t.string   "class_name"
    t.string   "location"
    t.integer  "parent_id"
    t.integer  "application_module_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["application_module_id"], name: "index_mks_auth_menus_on_application_module_id", using: :btree
    t.index ["parent_id"], name: "index_mks_auth_menus_on_parent_id", using: :btree
  end

  create_table "mks_auth_menus_user_roles", force: :cascade do |t|
    t.integer "menu_id"
    t.integer "user_role_id"
    t.index ["menu_id", "user_role_id"], name: "index_mks_auth_menus_user_roles_on_menu_id_and_user_role_id", using: :btree
  end

  create_table "mks_auth_user_roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mks_auth_users", force: :cascade do |t|
    t.string   "first_name",                           null: false
    t.string   "last_name",                            null: false
    t.string   "email",                                null: false
    t.boolean  "active",                default: true, null: false
    t.integer  "application_module_id"
    t.string   "password_digest"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["application_module_id"], name: "index_mks_auth_users_on_application_module_id", using: :btree
  end

  create_table "mks_auth_users_user_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "user_role_id"
    t.index ["user_id", "user_role_id"], name: "index_mks_auth_users_user_roles_on_user_id_and_user_role_id", using: :btree
  end

  add_foreign_key "mks_auth_menus", "mks_auth_application_modules", column: "application_module_id"
  add_foreign_key "mks_auth_menus", "mks_auth_menus", column: "parent_id"
  add_foreign_key "mks_auth_menus_user_roles", "mks_auth_menus", column: "menu_id"
  add_foreign_key "mks_auth_menus_user_roles", "mks_auth_user_roles", column: "user_role_id"
  add_foreign_key "mks_auth_users", "mks_auth_application_modules", column: "application_module_id"
  add_foreign_key "mks_auth_users_user_roles", "mks_auth_user_roles", column: "user_role_id"
  add_foreign_key "mks_auth_users_user_roles", "mks_auth_users", column: "user_id"
end
