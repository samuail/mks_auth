Mks::Auth::Engine.routes.draw do
	get '/csrf_token', to: 'access#csrf_token'

  get '/attempt_login', to: 'access#attempt_login'

  get '/logout', to: 'access#logout'

  get '/menu', to: 'access#menu'

  get '/check_login', to: 'access#check_login'

  post '/login', to: 'access#attempt_login'

  resources :application_modules

  # get '/users', to: 'users#index'

  resources :users, except: [:new, :edit, :show, :destroy]

  get '/users/fetch_by_role', to: 'users#fetch_by_role'

  # get '/user_roles', to: 'user_roles#index'

  resource :user_roles, except: [:new, :edit, :show, :destroy]

  post '/assign_roles', to: 'user_roles#assign_roles'

  get '/assigned_roles/:user_id', to: 'user_roles#get_assigned_roles'

end
