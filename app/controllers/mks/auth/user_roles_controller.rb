require_dependency 'mks/auth/application_controller'

module Mks
  module Auth
    class UserRolesController < ApplicationController
      before_action :confirm_logged_in

      # GET /user_roles
      def index
        @user_roles = UserRole.all.order(:name)
        response = { success: true, data: @user_roles }
        render json: response
      end

      def get_assigned_roles
        user = User.find(params[:user_id])
        user_roles = UserRole.all.order(:name)
        data = []
        user_roles.each do |user_role|
          item = {id: user_role.id, name: user_role.name}
          if user.roles.include? user_role
            item[:selected] = true
          else
            item[:selected] = false
          end
          data << item
        end
        response = { success: true, data: data }
        render json: response
      end

      def assign_roles
        user = User.find(params[:user_id])
        roles = params[:roles]

        roles.each do |role|
          user_role = UserRole.find role[:id]
          if role[:selected]
            user.roles << user_role
          else
            user.roles.destroy user_role
          end
        end

        user.save

        response = { success: true, message: 'Role assignment successful!' }
        render json: response
      end
    end
  end
end