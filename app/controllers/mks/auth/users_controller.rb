require_dependency 'mks/auth/application_controller'

module Mks
  module Auth
    class UsersController < ApplicationController
      before_action :set_user, only: [:update]

      def index
        @users = User.where(application_module_id: app_module.id)
        response = { success: true, data: @users }
        render json: response
      end

      def fetch_by_role
        r = params[:role]
        role = UserRole.find_by(name: r)
        unless role
          raise 'Role not found'
        end
        response = { success: true, data: role.users }
        render json: response
      end

      def create
        @user = User.new(user_params)
        @user.application_module_id = app_module.id
        if @user.save
          response = { success: true, message: 'User saved successfully' }
          render json: response
        else
          errors = Util.error_messages @user, 'User'
          response = { success: false, errors: errors }
          render json: response
        end
      end

      def update
        if @user.update(user_params)
          response = { success: true, message: 'User updated successfully' }
          render json: response
        else
          errors = Util.error_messages @user, 'User'
          response = { success: false, errors: errors }
          render json: response
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def user_params
        params.require(:user).permit(:first_name, :last_name, :email, :password)
      end
    end
  end
end