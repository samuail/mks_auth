module Mks
  module Auth
    class ApplicationController < ActionController::Base
      include AccessHelper
      include ApplicationHelper

      private

      def confirm_logged_in
        if session[:user_id]
          true
        else
          redirect_to '/'
          false
        end
      end

      protected

      def verified_request?
        super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
      end
    end
  end
end