module Mks
  module Auth
    class ApplicationModule < ApplicationRecord
      #self.table_name = 'mks_application_modules'

      validates :code, presence: true
      validates :code, presence: true, uniqueness: true

      has_many :users, class_name: 'Mks::Auth::User'
      has_many :menus, class_name: 'Mks::Auth::Menu'
    end
  end
end