module Mks
  module Auth
    class User < ApplicationRecord
      # self.table_name = 'mks_users'

      belongs_to :application_module, class_name: 'Mks::Auth::ApplicationModule'
      has_and_belongs_to_many :roles, class_name: 'Mks::Auth::UserRole', join_table: :mks_auth_users_user_roles
      has_secure_password

      before_save { email.downcase! }

      VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
      validates :first_name, presence: true, length: {maximum: 30}
      validates :last_name, presence: true, length: {maximum: 30}
      validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: {case_sensitive: false}
      validates :password, length: { minimum: 6 }
      validates :active, presence: true

      def full_name
        "#{first_name} #{last_name}"
      end
    end
  end
end