module Mks
  module Auth
    class UserRole < ApplicationRecord
       # self.table_name = 'mks_user_roles'

      validates :name, presence: true, uniqueness: true
      has_and_belongs_to_many :users, :join_table => :mks_auth_users_user_roles
      has_and_belongs_to_many :menus, :join_table => :mks_auth_menus_user_roles
    end
  end
end