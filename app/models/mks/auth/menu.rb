module Mks
  module Auth
    class Menu < ApplicationRecord
      # self.table_name = 'mks_menus'

      belongs_to :application_module, class_name: 'Mks::Auth::ApplicationModule'
      belongs_to :parent, class_name: 'Mks::Auth::Menu', optional: true
      has_many :children, class_name: 'Mks::Auth::Menu', :foreign_key => 'parent_id'
      has_and_belongs_to_many :roles, class_name: 'Mks::Auth::UserRole', :join_table => :mks_auth_menus_user_roles
    end
  end
end