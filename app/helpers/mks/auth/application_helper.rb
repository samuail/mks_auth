module Mks
  module Auth
    module ApplicationHelper
      def login_user(user)
        session[:user_id] = user.id
      end

      def current_user
        @current_user ||= User.find_by(id: session[:user_id])
      end

      def logged_in?
        !current_user.nil?
      end

      def logout_user
        session.delete(:user_id)
        @current_user = nil
        @menus = nil
      end

      def fetch_menus
        if @menus.nil?
          roles = current_user.roles
          app_module = current_user.application_module

          @menus = []
          roles.each do |role|
            if role
              menu_list = role.menus.where(:parent => nil, :application_module => app_module)
              menu_list.each do |menu|
                children = []
                menu.children.order(:text).each do |child|
                  if child.roles.include? role
                    children << {'text': child.text, 'className': child.class_name, 'iconCls': child.icon_cls}
                  end
                end
                @menus << {'text': menu.text, 'children': children, 'iconCls': menu.icon_cls}
              end
            end
          end
        end
        @menus
      end
    end
  end
end