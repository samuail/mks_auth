class CreateMksMenusUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :mks_auth_menus_user_roles do |t|
      t.references :menu, index: false
      t.references :user_role, index: false
    end
    add_index :mks_auth_menus_user_roles, [:menu_id, :user_role_id]
    add_foreign_key :mks_auth_menus_user_roles, :mks_auth_menus, :column => :menu_id
    add_foreign_key :mks_auth_menus_user_roles, :mks_auth_user_roles, :column => :user_role_id
  end
end
