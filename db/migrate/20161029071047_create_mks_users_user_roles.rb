class CreateMksUsersUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :mks_auth_users_user_roles, id: false do |t|
      t.references :user, index: false
      t.references :user_role, index: false
    end
    add_index :mks_auth_users_user_roles, [:user_id, :user_role_id]
    add_foreign_key :mks_auth_users_user_roles, :mks_auth_users, :column => :user_id
    add_foreign_key :mks_auth_users_user_roles, :mks_auth_user_roles, :column => :user_role_id
  end
end
