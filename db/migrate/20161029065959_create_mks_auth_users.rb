class CreateMksAuthUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :mks_auth_users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.boolean :active, null: false, default: true
      t.references :application_module, index: true
      t.string :password_digest

      t.timestamps
    end

    add_foreign_key :mks_auth_users, :mks_auth_application_modules, :column => :application_module_id
  end
end
