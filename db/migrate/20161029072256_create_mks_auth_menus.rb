class CreateMksAuthMenus < ActiveRecord::Migration[5.0]
  def change
    create_table :mks_auth_menus do |t|
      t.string :text, null: false
      t.string :icon_cls
      t.string :class_name
      t.string :location
      t.integer :parent_id, index: true
      t.references :application_module, index: true

      t.timestamps
    end

    add_foreign_key :mks_auth_menus, :mks_auth_menus, :column => :parent_id
    add_foreign_key :mks_auth_menus, :mks_auth_application_modules, :column => :application_module_id
  end
end
