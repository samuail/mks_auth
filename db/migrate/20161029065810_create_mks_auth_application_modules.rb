class CreateMksAuthApplicationModules < ActiveRecord::Migration[5.0]
  def change
    create_table :mks_auth_application_modules do |t|
      t.string :code, unique: true, null: false
      t.string :name, null: false

      t.timestamps
    end
  end
end
