class CreateMksAuthUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :mks_auth_user_roles do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
